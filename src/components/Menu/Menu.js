import React, {Component} from 'react';
import {connect} from "react-redux";
import {addFood, getProduct} from "../../store/actions/actionMaker";

import './Menu.css'

class Menu extends Component {
    componentDidMount() {
        this.props.getFoods()
    }

    render() {
        const foods = Object.keys(this.props.foods).map(food => {
            return {...this.props.foods[food]};
        });

        const food = foods.map((food, key) => {
            return (
                <div className="food" key={key}>
                    <img className='food-img'
                        src={food.image} alt="#"
                    />
                    <div className="info">
                        <h4 className='food-name'>{food.name}</h4>
                        <span className='food-price'>{food.price} <b>KGS</b></span>
                        <button className="add-btn"
                            onClick={() => this.props.addFood({name: food.name, price: food.price})}
                        >Add to cart</button>
                    </div>
                </div>
            )
        });


        return (
            <div className='menu'>
                {food}
            </div>
        );
    }

}

const mapStateToProps = state => ({
    foods: state.menuRed.foods
});

const mapDispatchToProps = dispatch => ({
    getFoods: () => dispatch(getProduct()),
    addFood: (food) => dispatch(addFood(food))
});

export default connect(mapStateToProps, mapDispatchToProps)(Menu);