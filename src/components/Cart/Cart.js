import React, {Component} from 'react';
import {connect} from "react-redux";

class Cart extends Component {
    render() {

        const cartElement = this.props.cart;
        console.log(cartElement);
        return (
            <div className="cart-elem">
                <h4 className='food-name'>{cartElement.name}</h4>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    cart: state.cartRed.cart
});


export default connect(mapStateToProps)(Cart);