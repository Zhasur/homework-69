import React, { Component } from 'react';
import './App.css';
import MenuBuilder from "./containers/MenuBuilder/MenuBuilder";

class App extends Component {
  render() {
    return (
      <div className="App">
        <MenuBuilder/>
      </div>
    );
  }
}

export default App;
