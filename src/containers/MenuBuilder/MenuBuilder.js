import React, {Component} from 'react';
import Menu from "../../components/Menu/Menu";
import Cart from "../../components/Cart/Cart";

import './MenuBuilder.css'

class MenuBuilder extends Component {
    render() {
        return (
            <div className='main-block'>
                <Menu/>
                <Cart/>
            </div>
        );
    }
}

export default MenuBuilder;