import {MENU_SUCCESS} from "../actions/actionTypes";

const initialState = {
    foods: {}
};

const reducerMenu = (state = initialState, action) => {
    switch (action.type) {
        case MENU_SUCCESS:
            return {...state, foods: action.foods};
        default:
            return state;
    }
};

export default reducerMenu;

