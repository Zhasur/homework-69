import {ADD_FOOD} from "../actions/actionTypes";

const initialState = {
    cart: {},
    totalPrice: 0,
};

const reducerMenu = (state = initialState, action) => {
    switch (action.type) {
        case ADD_FOOD:
            return {
                ...state,
                cart: {
                    ...state.cart,
                    [action.food.name]: state.cart[action.food.name] + 1
                },
                totalPrice: state.totalPrice + state.cart[action.food.price]
            };
        default:
            return state;
    }
};

export default reducerMenu;

