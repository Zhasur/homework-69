import {MENU_ERR, MENU_REQUEST, MENU_SUCCESS, ADD_FOOD, REMOVE_FOOD, INIT_FOOD} from "./actionTypes";
import axios from '../../axios-food';


export const menuRequest = () => ({type: MENU_REQUEST });
export const menuSuccess = (foods) => ({type: MENU_SUCCESS, foods});
export const menuError= (error) => ({type: MENU_ERR, error});

export const addFood = food => ({type: ADD_FOOD, food});
export const removeFood = foodName => ({type: REMOVE_FOOD, foodName});
export const initFood = () => ({type: INIT_FOOD});


export const getProduct = () => {
    return dispatch=> {
        dispatch(menuRequest());
        axios.get('product.json').then(response => {
            dispatch(menuSuccess(response.data));
            console.log(response.data)
        },error => {
            dispatch(menuError(error))
        })
    }
};