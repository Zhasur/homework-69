export const ADD_FOOD = 'ADD_FOOD';
export const REMOVE_FOOD = 'REMOVE_FOOD';
export const INIT_FOOD = 'INIT_FOOD';

export const MENU_REQUEST = 'ORDER_REQUEST';
export const MENU_SUCCESS = 'ORDER_SUCCESS';
export const MENU_ERR = 'ORDER_FAILURE';
